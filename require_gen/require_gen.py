#! python
# IPython log file
# Brian Volk
# Requirements Generator

import jinja2

def get_setup():
    output_file = raw_input("Output file: ")
    title = raw_input("Page title: ")
    prev_file = raw_input("Prev file: ")
    next_file = raw_input("Next file: ")
    topic_file = raw_input("Section file: ")
    major = raw_input("Major Version number (the x in x.2.2): ")
    minor = raw_input("Minor Version number (the x in 2.x.2): ")
    mode = raw_input("Create entries [Y/n] (n will accept the full content as an input filename and only do the framework (header/footer) design): ")
    return { "output_file": output_file,
             "title": title,
             "prev_file": prev_file,
             "next_file": next_file,
             "topic_file": topic_file,
             "prev_name": prev_file[:-5],
             "next_name": next_file[:-5],
             "topic_name": topic_file[:-5],
             "major": major,
             "minor": minor }, mode

def new_entry():
    title = raw_input("Title (can be blank): ")
    last_description = raw_input("Description (can include new_lines and html) enter nothing to end: ")
    description = last_description
    while last_description:
        last_description = raw_input("Description (cont'd): ")
        description += "\n" + last_description
    caption = raw_input("Caption for image, required: ")
    return { "title": title, "description": description, "caption": caption }


def main():
    template = jinja2.Template(open("require_template.html",'r').read())
    settings, mode = get_setup()

    out = ""

    if 'N' in mode or 'n' in mode:
        filename = raw_input("Filename of content (should be just body content, no header stuff): ")
        try:
            f = open(filename,'r')
            settings["content"] = f.read()
            out = template.render(settings)
        except:
            pass
    else:
        entries = []
        c = ""
        print "Begin entry input\n"
        while not ("n" in c or "N" in c):
            entries.append(new_entry())
            c = raw_input("To end enter 'n': ")
        settings["entries"] = entries
        out = template.render(settings)

    o = open(settings['output_file'],'w')
    o.write(out)
    o.close()
    # why not use stdout as a backup ya feel
    print out

if __name__ == "__main__":
    main()

