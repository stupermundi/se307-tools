# Easy requirements HTML generator for SE307

### Install prereqs

Only one prereq - Jinja2 which is used as a templating language

Either in a [virtualenv](https://virtualenv.pypa.io/en/latest/) (recommended) or globally (not recommended)
```
pip install -r requirements.txt
```
or
```
<python_package_installer_of_choice> jinja2
```

### To Run

No command line args yet.

```
python require_gen.py
```

### Notes
 - `(next|prev|section)_file` are expected to be in 'filename.html' format
 - To have multiple paragraphs, just include additional <p> tags in the description, as there is always an image and caption included.
 - image names are fig_<major>_<minor>_<auto>.png
 - title is a name on the section that might allow for permalinking (maybe, but it was in the template)
