# Review table generation tool

### Install

Nothing to do!

### Run

```
python review_gen.py
```

### Notes
 - Major version is x in x.2.2
 - Minor version is x in 2.x.2
 - Micro version is x in 2.2.x
 - Type is `P`,`I`,`A` or whatever you want
 - Status is `DONE`, `FIX`, or something similar
 - All of the inputs aren't html escaped so remember to use &lt; and &gt; for tags